//George Miro
//CS 445 - COMPUTER SCIENCE
//HOMEWORK 2 - QUESTION 3
//Coin Class
function Coin(name, value){
   this.name = name;
   this.value = value;
   this.getInfo = function(){
    console.log("Name: " + this.name + " value: " + this.value);
  }
}

//Wallet Class
function Wallet()
{
  this.purse = new Array();
  this.coinCount = new Array();
  this.numOfCoins = 0;

  //adding a coin to the wallet
  //first we check if the coin type already exist in the Wallet by using Search Method
  //if not exist we add it and increase the frequency
  this.addCoin = function(coinx){
    var coinExist = this.searchWallet(coinx);
    if (coinExist >=0){
      this.coinCount[coinExist]++;
    }
    else{
      this.purse[this.purse.length] = coinx;
      this.coinCount[this.coinCount.length] =1;
    }
    this.numOfCoins++;
  }
  //search the wallet for a coin by using the equals method
  this.searchWallet = function(coinx){
    for(var i=0; i< this.purse.length; i++){
      if (this.equals(coinx,this.purse[i])){
        return i;
      }
    }
  }
  //print the name, value and the frequency for each Coin in the Wallet
  this.print = function(){
    for (var i = 0; i < this.purse.length; i++) {
      console.log(this.purse[i].getInfo());
      console.log(this.coinCount[i]);
    }
  }

  this.getNumOfCoins = function(){
    return this.numOfCoins;
  }

  this.equals = function(c1,c2){
    if (c1.name === c2.name){
      return true;
    }
    return false;
  }
}

var w = new Wallet();
var coind = new Coin("dime", 10);
var coind2 = new Coin("dime",10);
var coinc = new Coin("cent",1);
w.addCoin(coinc);
w.addCoin(coind);
w.addCoin(coind2);
w.addCoin(coind2);
w.print();

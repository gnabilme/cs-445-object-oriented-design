{\rtf1\ansi\ansicpg1252\cocoartf1265\cocoasubrtf210
{\fonttbl\f0\fswiss\fcharset0 Helvetica;}
{\colortbl;\red255\green255\blue255;}
\margl1440\margr1440\vieww10800\viewh8400\viewkind0
\pard\tx720\tx1440\tx2160\tx2880\tx3600\tx4320\tx5040\tx5760\tx6480\tx7200\tx7920\tx8640\pardirnatural

\f0\fs24 \cf0 //README\
//George Miro\
//CS 445\
//HW2\
//WALLET COIN JAVASCRIPT\
\
To run the script file into the browser console:\
\
1- Open the developer mode console in the browser.\
2- Open the "coin_wallet.js" and copy the code into the console.\
3- Execute the code in console.\
4- The program will create three coins (Cent and two Dimes)\
5- The print method will display the results.\
\
}
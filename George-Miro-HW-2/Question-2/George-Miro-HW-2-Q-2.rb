class A
  # class A depends on B
  def initialize(bb = B.new())
    @bb = bb
  end
end

class B
  def initialize()

  end
end

class C < A
  #class C depends on D
  #class C is a subclass of class A
  def initialize(dd = D.new())
    @dd = dd
  end
end

class D
  #class D uses class B and F
  bb = B.new()
  ff = [F.new(), F.new(), F.new(), F.new(), F.new()]

  def initialize()

  end

  def getB
    @bb
  end

  def getF
    @ff
  end

  def setB(value)
    @bb = value
  end

  def setF(value)
    @ff = value
  end
end

class E < C
  #class E is a subclass of class C
  def initialize()

  end
end

class F
  #class F uses class D
  dd = [D.new(), D.new()]

  def initialize()

  end

  def getD
    @dd
  end

  def setD(value)
    @dd = value
  end
end

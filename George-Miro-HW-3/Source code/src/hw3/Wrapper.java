package hw3;

public class Wrapper {
	public static void main (String [] args) {
		
		ImprovedRandom rnd = new ImprovedRandom(); 
		
		if(rnd.getRandInRange(0, 1) >= 0 && rnd.getRandInRange(0, 1) < 1)
			System.out.println("Random number is " +rnd.getRandInRange(0, 1) +" between 0 and 1");
		if(rnd.getRandInRange(50, 80) >= 50 && rnd.getRandInRange(50, 80) < 80)
			System.out.println("Random number is " +rnd.getRandInRange(50, 80) +" between 50 and 80");
		if(rnd.getRandInRange(35, 60) >= 35 && rnd.getRandInRange(35, 60) < 60)
			System.out.println("Random number is " +rnd.getRandInRange(35, 60) +" between 35 and 60");			
		
	}
}

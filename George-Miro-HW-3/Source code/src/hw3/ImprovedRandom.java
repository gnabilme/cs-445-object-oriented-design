package hw3;

import java.util.Random;

public class ImprovedRandom extends Random {

	public ImprovedRandom() {
		
	}
	public ImprovedRandom(long seed) {
		super(seed);  
	}
	
	public int getRandInRange(int a, int b) { 
		
		return nextInt(b-a) + a;
	}
	
}

package hw3;
import java.util.StringTokenizer; 

public class ImprovedStringTokenizer extends StringTokenizer {
	
	public ImprovedStringTokenizer(String str) {
		super(str);
	}
	
	public String[] getTokenArray() {
		String[] arr = new String[countTokens()]; 
		for (int i = 0; i < arr.length; i++) {
			arr[i] = nextToken();
		}
		
		return arr;
	}
	
}

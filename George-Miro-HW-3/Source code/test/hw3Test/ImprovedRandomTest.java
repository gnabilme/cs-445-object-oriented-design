package hw3Test;

import static org.junit.Assert.*;
import hw3.ImprovedRandom;

import org.junit.Test;

public class ImprovedRandomTest {

	@Test
	public void getRandInRangeTest() {
		ImprovedRandom rnd = new ImprovedRandom();
		assertTrue(rnd.getRandInRange(0, 1) >= 0 && rnd.getRandInRange(0, 1) < 1);
		assertTrue(rnd.getRandInRange(50, 80) >= 50 && rnd.getRandInRange(50, 80) < 80);
		assertTrue(rnd.getRandInRange(35, 60) >= 35 && rnd.getRandInRange(35, 60) < 60);
	}

}

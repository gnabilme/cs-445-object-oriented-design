package hw3Test;

import static org.junit.Assert.*;
import hw3.ImprovedStringTokenizer;

import org.junit.Test;

public class ImprovedStringTokenizerTest {

	@Test
	public void getTokenArrayTest() {
		ImprovedStringTokenizer tokenArray = new ImprovedStringTokenizer("This class is easy");
		ImprovedStringTokenizer tokenArray1 = new ImprovedStringTokenizer("Hello World!"); 
		String[] str = tokenArray.getTokenArray();
		String[] str1 = tokenArray1.getTokenArray();
		
		assertEquals("This", str[0]);
		assertEquals("class", str[1]);
		assertEquals("is", str[2]);
		assertEquals("easy", str[3]);
		assertEquals("Hello", str1[0]);
		assertEquals("World!", str1[1]);
		
	}

}

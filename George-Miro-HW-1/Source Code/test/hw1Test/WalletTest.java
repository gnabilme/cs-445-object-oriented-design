package hw1Test;

import static org.junit.Assert.*;

import java.util.Scanner;

import hw1.Coin;
import hw1.Wallet;

import org.junit.Before;
import org.junit.Test;

public class WalletTest {

	static Wallet wallet;
	
	@Before
	public void initialize() {
		wallet = new Wallet();
	}
	
	@Test
	public void addNonExistingCoinTest() {
		wallet = new Wallet();
		Coin coin = new Coin("penny", "1");
		wallet.add(coin);
		Coin coin1 = new Coin("dime", "10");
		wallet.add(coin1);

		assertEquals(2, wallet.getCoins().size());
		assertEquals(2, wallet.getCount().size());
		assertEquals(1, wallet.getCount().get(0).intValue());
		assertEquals(1, wallet.getCount().get(1).intValue());
	}

	@Test
	public void addExistingCoinTest() {
		wallet = new Wallet();
		Coin coin = new Coin("penny", "1");
		wallet.add(coin);
		Coin coin1 = new Coin("Penny", "1");
		wallet.add(coin1);
		Coin coin2 = new Coin("Penny", "2");
		wallet.add(coin2);
		
		assertEquals(2, wallet.getCoins().size());
		assertEquals(2, wallet.getCount().size());
		assertEquals(2, wallet.getCount().get(0).intValue());
		assertEquals(1, wallet.getCount().get(1).intValue());
	}
	
	public static void main(String[] args) {
		
		wallet = new Wallet(); 
		Scanner scan = new Scanner(System.in); 
		System.out.println("Please enter coin name");
		String name = "";
		while(!(name = scan.nextLine()).equals("")) {	
			System.out.println("Please enter coin value");
			String value = scan.nextLine(); 
			Coin coin = new Coin(name, value); 
			wallet.add(coin);
			wallet.printContent();
			System.out.println("Please enter coin name");
		}
		
		scan.close(); 
	}
}

package hw1;

import java.util.ArrayList;

public class Wallet {

	// data members
	private ArrayList<Coin> coins = new ArrayList<Coin>();
	private ArrayList<Integer> count = new ArrayList<Integer>(); 
	
	// methods 
	public void add(Coin coin) {

		int index = checkCoin(getCoins(), getCount(), coin);
		if(index >= 0) {
			getCount().set(index, getCount().get(index) + 1);
		} 
		else {
			getCoins().add(coin); 
			getCount().add(1);
		}
	}
	
	private static int checkCoin(ArrayList<Coin> coins, ArrayList<Integer> count, Coin coin) {
		
		for(int i = 0; i<coins.size(); i++) {
			if(compare(coins.get(i), coin))
				return i; 
		}
		
		return -1; 
	}
	
	public void printContent() {
		
		System.out.println();
		for(int i = 0; i<getCoins().size(); i++) {
			String name = getCoins().get(i).getName(); 
			String value = getCoins().get(i).getValue();
			int freq = getCount().get(i);
			System.out.println(name + " (" + value + "): " + freq);
		}
		System.out.println();
	}
	
	private static boolean compare(Coin coin1, Coin coin2) {
		return coin1.getName().equals(coin2.getName()) && coin1.getValue().equals(coin2.getValue());
	}

	public ArrayList<Coin> getCoins() {
		return coins;
	}

	public void setCoins(ArrayList<Coin> coins) {
		this.coins = coins;
	}

	public ArrayList<Integer> getCount() {
		return count;
	}

	public void setCount(ArrayList<Integer> count) {
		this.count = count;
	}
}

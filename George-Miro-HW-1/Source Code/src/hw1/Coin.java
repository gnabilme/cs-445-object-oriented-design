package hw1;

public class Coin {

	// data members 
	private String name;
	private String value; 

	// constructor 
	public Coin(String name, String value) {
		this.name = name.trim().toLowerCase(); 
		this.value = value.trim().toLowerCase(); 
	}
	
	// getters and setters
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
}
